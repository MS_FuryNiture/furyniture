package de.itestra.creators_contest_2022;

import de.itestra.creators_contest_2022.api.RatingApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class Test {

    @Autowired
    private RatingApi ratingApi;

    @org.junit.jupiter.api.Test
    public void test(){
        ratingApi.register();
    }
}
