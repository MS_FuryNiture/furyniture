package de.itestra.creators_contest_2022;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import java.awt.image.BufferedImage;

public class ImageToText {
    private Tesseract tesseract;

    public ImageToText(){
        tesseract = new Tesseract();
        tesseract.setDatapath("C:/Users/Eduardo/Downloads/creators-contest-2022/Tess4J/tessdata");
        tesseract.setLanguage("deu");
    }

    public String getText(BufferedImage image){
        try {

            return tesseract.doOCR(image);
        }
        catch (TesseractException e) {
            e.printStackTrace();
        }
        return "";
    }
}
