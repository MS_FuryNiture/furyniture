package de.itestra.creators_contest_2022.api;

import de.itestra.creators_contest_2022.ImageToText;
import de.itestra.creators_contest_2022.imageOps.ImageCropper;
import de.itestra.creators_contest_2022.model.*;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

@RestController
public class SolverAPI implements StudentsApi {

    @Override
    public ResponseEntity<Void> ping() throws Exception {
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Solution> solve(Resource body) throws Exception {

        ImageToText imageToText = new ImageToText();
        //Das folgende muss das received Image nehmen
        BufferedImage originalImage = ImageIO.read(body.getInputStream());

        List<String> features = new ArrayList<>();
        try {
            List<BufferedImage> columnImages = ImageCropper.getColumnImages(originalImage);
            features = ImageCropper.getFeaturesFromColumn(imageToText, columnImages);
        }catch (Exception e){
            System.out.println(e);
        }
        List<Feature> featureList = new ArrayList<>();
        try {
            featureList = featureStringToFeatureObject(features);
        }catch (Exception e){
            System.out.println(e);
        }
        Feature bezugFeature = new Feature();
        bezugFeature.setNameInFormula("Bezug");
        featureList.add(bezugFeature);



        return ResponseEntity.ok(new Solution()
                .product(new Product()
                        .prices(new ArrayList<>())
                        .features(featureList))
                .hints(new ArrayList<>()));
    }

    private static List<Feature> featureStringToFeatureObject(List<String> features) {
        String name = features.get(0).split("\n")[1].split(" ")[1];//Groesse

        Feature f = new Feature();
        f.nameInFormula(name);

        OptionRange optionRange = new OptionRange();
        optionRange.setName(name);

        List<Option> options = new ArrayList<>();

        for(String featureString : features){
            Option option = new Option();
            String optionString = featureString.split("\n")[1].split(" ")[2];
            option.setName(optionString);
            options.add(option);
        }

        optionRange.setOptions(options);
        f.addOptionRangesItem(optionRange);

        List<Feature> featureList =new ArrayList<>();
        featureList.add(f);
        return featureList;
    }

}

