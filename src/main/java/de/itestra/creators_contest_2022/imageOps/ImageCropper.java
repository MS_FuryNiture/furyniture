package de.itestra.creators_contest_2022.imageOps;

import de.itestra.creators_contest_2022.ImageToText;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImageCropper {
    public static List<BufferedImage> getColumnImages(BufferedImage originalImage) {
        Rectangle horizontalRect = new Rectangle(0,388,originalImage.getWidth(),1);
        BufferedImage schnippsel = originalImage.getSubimage(horizontalRect.x, horizontalRect.y, horizontalRect.width, horizontalRect.height);

        List<Integer> edgeKoordinates = findHorizontalEdges(schnippsel);

        return cropVertical(originalImage, edgeKoordinates);
    }
    private static List<Integer> findHorizontalEdges(BufferedImage schnippsel){

        List<Integer> lines = new ArrayList<Integer>();
        boolean lastWasBlack=false;
        for (int x = 0; x<schnippsel.getWidth();x++){
            int color = schnippsel.getRGB(x, 0);
            if(isBlack(color)){
                if(!lastWasBlack) {
                    lines.add(x);
                    lastWasBlack = true;
                }
            }
            else{
                lastWasBlack = false;
            }
        }
        return lines;
    }
    private static List<BufferedImage> cropVertical(BufferedImage src, List<Integer> edgeKoordinates)  {
        List<BufferedImage> croppedColumns = new ArrayList<>();
        for (int i = 0; i < edgeKoordinates.size()-1; i++) {
            Rectangle rect = new Rectangle(edgeKoordinates.get(i),346, edgeKoordinates.get(i+1)- edgeKoordinates.get(i),1000);
            BufferedImage teilImage = src.getSubimage(rect.x, rect.y, rect.width, rect.height);
            croppedColumns.add(teilImage);

        }
        return croppedColumns;
    }
    private static List<Integer> findVerticalEdges(BufferedImage schnippsel){

        List<Integer> lines = new ArrayList<Integer>();
        boolean lastWasBlack=false;
        for (int y = 0; y<schnippsel.getHeight();y++){
            int color = schnippsel.getRGB(0, y);
            if(isBlack(color)){
                if(!lastWasBlack) {
                    lines.add(y);
                    lastWasBlack = true;
                }
            }
            else{
                lastWasBlack = false;
            }
        }
        return lines;
    }
    public static List<String> getFeaturesFromColumn(ImageToText imageToText, List<BufferedImage> columnImages) throws IOException {
        List<String> features = new ArrayList<>();
        List<BufferedImage> featureImages = new ArrayList<>();
        for(BufferedImage imageSlice : columnImages){
            Rectangle verticalRect = new Rectangle(3,0 ,1, imageSlice.getHeight());
            BufferedImage verticalImage = imageSlice.getSubimage(verticalRect.x,verticalRect.y,verticalRect.width,verticalRect.height);
            List<Integer> vertCoordinates= findVerticalEdges(verticalImage);

            BufferedImage featureImage = cropHorizontal(imageSlice, vertCoordinates);
            String recognizedText = imageToText.getText(featureImage);

            System.out.println(recognizedText);
            if(recognizedText.length() >= 3){
                ImageIO.write(featureImage,"png",new File("images/feature-"+recognizedText.replaceAll("\\W+", "")+".png"));
                features.add(recognizedText);
                featureImages.add(featureImage);
            }
        }
        return features;
    }


    private static BufferedImage cropHorizontal(BufferedImage imageSlice, List<Integer> vertCoordinates) {

        Rectangle rect = new Rectangle(0,vertCoordinates.get(0),imageSlice.getWidth(),vertCoordinates.get(1)- vertCoordinates.get(0));
        return imageSlice.getSubimage(rect.x,rect.y,rect.width,rect.height);
    }
    private static boolean isBlack(int color){
        int blue = color & 0xff;
        int green = (color & 0xff00) >> 8;
        int red = (color & 0xff0000) >> 16;
        return blue+red+green <30;
    }
}
