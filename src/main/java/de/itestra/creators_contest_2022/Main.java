package de.itestra.creators_contest_2022;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.itestra.creators_contest_2022.imageOps.ImageCropper;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.http.HttpClient;
import java.util.List;


class Main{





    public static void main(String[] args) throws IOException {

        ImageToText imageToText = new ImageToText();
        BufferedImage originalImage = ImageIO.read(new File("images/635403a3dc78dc3de79c6a03.png"));

        List<BufferedImage> columnImages = ImageCropper.getColumnImages(originalImage);
        List<String> features = ImageCropper.getFeaturesFromColumn(imageToText, columnImages);






        System.out.println("Success!");
       /* ApiClient apiClient = new ApiClient("https://creators-contest-2022.itestra.de/auth/realms/creators-contest-2022/protocol/openid-connect/token","fury-niture","48a5128f-7b2d-4b2b-8b00-32c102390624",new HashMap<>());
        //apiClient.set
        RatingApi api = new RatingApi();
        api.register();
*/


        /*String response = fetchJson(); // Gibt im Wesentlichen die JSON-Antwort von der Itestra Schnittstelle zurück
        String imageAsBase64 = getValueFromJSON(response, "task");// Holt nur den Image String aus der JSON
        String id = getValueFromJSON(response, "id"); // Wie oben, nur mit ID
        saveBase64Image(imageAsBase64, id);*/
    }





    private static void saveBase64Image(String imageAsBase64, String id) {
        byte[] data = DatatypeConverter.parseBase64Binary(imageAsBase64);
        id = id.replaceAll("\"", "");//ID Steht in anführungszeichen, die wollen wir nicht, da der Pfad sonst ungültig wird.
        String path = "./images/"+id+".png";
        System.out.println("Saving to: " + path);
        File file = new File(path);
        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
            outputStream.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getValueFromJSON(String in,String value) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode;
        try {
            rootNode = mapper.readTree(in);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return rootNode.get(value).toString();

    }

    private static String fetchJson() throws IOException {
        HttpClient client = HttpClient.newHttpClient();
        URL url = new URL("http://creators-contest-2022.itestra.de/api/tasks/EASY");
        HttpURLConnection conn = null;
        BufferedReader in = null;
        String token = "";
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization","Bearer "+token);


            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        } catch (ProtocolException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        String output;

        StringBuffer response = new StringBuffer();
        while ((output = in.readLine()) != null) {
            response.append(output);
        }

        in.close();
        // printing result from response
        System.out.println("Response:-" + response.toString());

        return response.toString();
    }
}