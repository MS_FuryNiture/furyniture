FROM openjdk:17-jdk-alpine

# Install tesseract library
RUN apk add --no-cache tesseract-ocr
# Download last language package
ADD https://github.com/tesseract-ocr/tessdata/raw/main/deu.traineddata /usr/share/tessdata/deu.traineddata
# Check the installation status
ENV TESSDATA_PREFIX=/usr/share/tessdata/
RUN tesseract --list-langs
RUN tesseract -v
# Set the name of the jar
ENV APP_FILE *.jar

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar", "-Dspring.profiles.active=prod"]