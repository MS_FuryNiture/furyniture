#!/bin/bash
# usage: deploy.sh
date=$(date +%s)
helm upgrade --install --namespace team-fury-niture --values "deployment/values.gcloud.yaml" --set metadata.date="$date" solution deployment/chart
